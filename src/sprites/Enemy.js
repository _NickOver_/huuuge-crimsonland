export default class Enemy extends Phaser.GameObjects.Image {
    constructor(scene) {
        super(scene, 0, 0, 'enemy');
        scene.add.existing(this);
        this.alive = true;
        console.log('asd')
        console.log(this)

        this.speed = Phaser.Math.GetSpeed(200, 1);
    }

    spawn() {
        let startingPosition = Math.floor(Math.random() * (3 - 0)) + 0;
        switch(startingPosition) {
            case 0:
                this.spawnAtPosition(-32, 300)
                break;
            case 1:
                this.spawnAtPosition(400, -32)
                break;
            case 2:
                this.spawnAtPosition(832, 300)
                break;
            case 3:
                this.spawnAtPosition(400, 332)
                break;
        }
    }

    spawnAtPosition(x, y) {
        this.setActive(true);
        this.setVisible(true);
        this.setPosition(x, y);
    }

    update(time, delta)
    {
        //TODO Fix enemies parkinson  
        var radians = Phaser.Math.Angle.Between(this.x, this.y, this.scene.player.x, this.scene.player.y);
        var angle = radians * (180/Math.PI)
        this.angle = angle
        this.x += Math.cos(angle) * (this.speed * delta);
        this.y += Math.sin(angle) * (this.speed * delta);

        if(this.alive) {
            this.lifespan -= delta;
            if (this.lifespan <= 0) {
                this.kill()
            }
        } else {
            if((this.x > 800 && this.x < 0) || (this.y > 600 && this.y < 0)) {
                this.kill();
            }
        }
    }

    kill() {
        this.alive = false;
        this.setActive(false);
        this.setVisible(false);
        this.destroy();
    }
}
