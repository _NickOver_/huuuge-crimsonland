export default class Bullet extends Phaser.GameObjects.Image {
 
    constructor(scene) {

        super(scene, 0, 0, 'bullet');
        scene.add.existing(this);;
 
        this.dx = 0;
        this.dy = 0;
        this.lifespan = 0;
        this.hasLifespan = false;
 
        this.speed = Phaser.Math.GetSpeed(600, 1);
    }

    fire(x, y, angle)
    {
        this.setActive(true);
        this.setVisible(true);

        this.setPosition(x, y);
 
        this.dx = Math.cos(angle);
        this.dy = Math.sin(angle);
 
        this.lifespan = 300;
    }
 
    update(time, delta)
    { 
        this.x += this.dx * (this.speed * delta);
        this.y += this.dy * (this.speed * delta);

        if(this.hasLifespan) {
            this.lifespan -= delta;
            if (this.lifespan <= 0) {
                this.destroyBullet()
            }
        } else {
            if((this.x > 800 && this.x < 0) || (this.y > 600 && this.y < 0)) {
                this.destroyBullet();
            }
        }
    }

    destroyBullet() {
        this.setActive(false);
        this.setVisible(false);
    }
 
}