class TitleScene extends Phaser.Scene {

    constructor(test) {
        super({
            key: 'TitleScene'
        });
    }

    create() {
        this.add.bitmapText(260, 124, 'font', 'PRESS X TO START', 18);

        this.input.keyboard.on('keydown_X', function (event) {
            console.log('pressed')
            this.startGame();
        }, (this));
    }

    startGame() {
        this.scene.stop('TitleScene');
        this.scene.start('GameScene');
    }
}

export default TitleScene;
