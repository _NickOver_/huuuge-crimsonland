class BootScene extends Phaser.Scene {
    constructor(test) {
        super({
            key: 'BootScene'
        });
    }
    preload() {
        const progress = this.add.graphics();

        this.load.on('progress', (value) => {
            progress.clear();
            progress.fillStyle(0xFFFFFF, 1);
            progress.fillRect(0, this.sys.game.config.height / 2, this.sys.game.config.width * value, 60);
        });

        this.load.on('complete', () => {
            progress.destroy();
            this.scene.start('TitleScene');
        });

        this.load.image('background', 'assets/images/background.png')
        this.load.image('crosshair', 'assets/images/crosshair.png')
        this.load.image('bullet', 'assets/images/bullet.png')

        this.load.image('enemy', 'assets/images/enemy.png')

        this.load.spritesheet('hero', 'assets/images/hero-handgun.png', {
            frameWidth: 39,
            frameHeight: 29
        });

        this.load.bitmapFont('font', 'assets/fonts/font.png', 'assets/fonts/font.fnt');
    }
}

export default BootScene;
