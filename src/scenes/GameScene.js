import Bullet from '../sprites/Bullet';
import Enemy from '../sprites/Enemy';

class GameScene extends Phaser.Scene {

    constructor(test) {
        super({
            key: 'GameScene'
        });
    }

    create() {
        this.keys = {
            top: this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.W),
            down: this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.S),
            left: this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.A),
            right: this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.D),
        };

        this.timeFromLastSpawn = 0;

        this.fillBackground()
        this.createPlayer()
        this.createObjects()
        this.enablePhysics()
        this.bindKeyboardInputHandling();
        this.bindMouseInputHandling();
    }

    fillBackground() {
        this.add.tileSprite(0, 0, this.sys.game.config.width * 2, this.sys.game.config.height * 2, 'background');
    }

    createPlayer() {
        this.player = this.physics.add.sprite(this.sys.game.config.width / 2 - 16, this.sys.game.config.height / 2 - 16, 'hero');
        this.player.setOrigin(0.5, 0.5).setDisplaySize(39, 29).setCollideWorldBounds(true).setDrag(100, 100);
    }

    createObjects() {
        this.crosshair = this.add.tileSprite(this.sys.game.config.width / 2 - 16, this.sys.game.config.height / 2 - 16, 32, 32, 'crosshair')
        this.bullets = this.physics.add.group({
            classType: Bullet,
            runChildUpdate: true
        })
        this.enemies = this.physics.add.group({
            classType: Enemy,
            runChildUpdate: true
        })
    }
    
    enablePhysics() {
        this.physics.world.setBounds(0, 0, this.sys.game.config.width, this.sys.game.config.height);
        this.physics.add.overlap(this.enemies, this.bullets, this.enemyHit);
        this.physics.add.overlap(this.enemies, this.player, this.playerHit, function(){return true;}, this);
    }

    update (time, delta)
    {
        if(this.shoudSpawnEnemies(delta)) {
            this.spawnEnemies()
        }
    }

    bindKeyboardInputHandling() {
        let self = this;
        this.input.keyboard.on('keydown_W', function (event) {
            self.player.setAccelerationY(-800);
        });
        this.input.keyboard.on('keydown_S', function (event) {
            self.player.setAccelerationY(800);
        });
        this.input.keyboard.on('keydown_A', function (event) {
            self.player.setAccelerationX(-800);
        });
        this.input.keyboard.on('keydown_D', function (event) {
            self.player.setAccelerationX(800);
        });

        this.input.keyboard.on('keyup_W', function (event) {
            if (self.keys['down'].isUp)
            self.player.setAccelerationY(0);
        });
        this.input.keyboard.on('keyup_S', function (event) {
            if (self.keys['top'].isUp)
            self.player.setAccelerationY(0);
        });
        this.input.keyboard.on('keyup_A', function (event) {
            if (self.keys['right'].isUp)
            self.player.setAccelerationX(0);
        });
        this.input.keyboard.on('keyup_D', function (event) {
            if (self.keys['left'].isUp)
                self.player.setAccelerationX(0);
        });
    }

    bindMouseInputHandling() {
        let self = this;

        this.game.canvas.addEventListener('mousedown', function () {
            self.game.input.mouse.requestPointerLock();
        });

        this.input.on('pointermove', function (pointer) {
            if (self.input.mouse.locked)
            {
                self.updateCrosshairPosition(pointer);
                self.updatePlayerAngle()
            }
        }, this);

        this.input.on('pointerup', function (pointer) {
            if (self.input.mouse.locked)
            {
                this.shoot();
            }
        }, this);
    }

    updateCrosshairPosition(pointer) {
        if (this.input.mouse.locked) {
            this.crosshair.x += pointer.movementX;
            this.crosshair.y += pointer.movementY;
        }
    }

    shoot() {
        var angle = Phaser.Math.Angle.Between(this.player.body.position.x + 32, this.player.body.position.y + 32, this.crosshair.x, this.crosshair.y);
        this.addBullet(this.player.body.position.x + 20, this.player.body.position.y + 15, angle);
    }

    addBullet(x, y, angle) {
        var bullet = this.bullets.get();
        if (bullet)
        {
            bullet.fire(x, y, angle);
        }
    }

    updatePlayerAngle() {
        var angle = Phaser.Math.Angle.Between(this.player.body.position.x + 16, this.player.body.position.y + 32, this.crosshair.x, this.crosshair.y);
        this.player.angle = this.radiansToDegree(angle)
    }

    radiansToDegree(radians) {
        return radians * (180/Math.PI); 
    }

    shoudSpawnEnemies(delta) {
        this.timeFromLastSpawn += delta
        if(this.timeFromLastSpawn >= 1000) {
            this.timeFromLastSpawn = 0
            return true;
        }
        return false;
    }

    spawnEnemies() {
        var enemy = this.enemies.get();
        if (enemy)
        {
            console.log('spawn');
            enemy.spawn();
        }
    }

    enemyHit(enemy, bullet) {
        if(enemy.active === true && bullet.active === true) {
            enemy.setActive(false)
            enemy.setVisible(false)
            bullet.setActive(false)
            bullet.setVisible(false)
        }
    }

    playerHit(enemy, player) {
        this.scene.stop('GameScene');
        this.scene.start('TitleScene');
    }
}

export default GameScene;
